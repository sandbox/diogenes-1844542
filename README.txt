The "Baboons Beware" (BB) module is a companion module to the "When Pigs Fly"
(WPF) module. Instructions on its use and utility can be found in the WPF
README.txt file.
